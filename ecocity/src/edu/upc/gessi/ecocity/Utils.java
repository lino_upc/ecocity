package edu.upc.gessi.ecocity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import edu.upc.gessi.ecocity.model.Globals;
import edu.upc.gessi.ecocity.model.NotificationItem;
import edu.upc.gessi.ecocity.model.WasteContainer;
import edu.upc.gessi.ecocity.model.WasteContainerStats;
import edu.upc.gessi.ecocity.model.WasteContainerType;

public class Utils
{
    private static final String DATA_FILE = "ecocity.dat";

    public static Spinner setSpinnerData(Activity activity, 
                                         int spinnerId, 
                                         int spinnerValuesResourceId)
    {
        Spinner spinner = (Spinner)activity.findViewById(spinnerId);
        ArrayAdapter<CharSequence> adapter 
            = ArrayAdapter.createFromResource(activity, spinnerValuesResourceId, 
                                              android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);  
        return spinner;
    }
    
    public static <T> Spinner setSpinnerData(Activity activity, 
                                         int spinnerId, 
                                         List<T> values)
    {
        Spinner spinner = (Spinner)activity.findViewById(spinnerId);
        ArrayAdapter<T> adapter 
            = new ArrayAdapter<T>(activity, 
                                  android.R.layout.simple_spinner_item,
                                  values);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);  
        return spinner;
    }

    public static void shortToast(Context context, int resId)
    {
        Toast.makeText(context, context.getText(resId), Toast.LENGTH_SHORT).show();    
    }
    
    public static void shortToast(Context context, String text)
    {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();    
    }

    public static void longToast(Context context, String text)
    {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();    
    }
    
    public static void setStaticText(Activity activity, int resourceId, String text)
    {
        TextView textView = (TextView)activity.findViewById(resourceId);
        textView.setText(text);
        textView.setVisibility(View.VISIBLE);
    }
    
    public static String getStringProperty(JsonObject jsonObj, String propertyName)
    {
        return jsonObj.getAsJsonPrimitive(propertyName).getAsString();
    }
    
    public static Integer getIntegerProperty(JsonObject jsonObj, String propertyName)
    {
        return jsonObj.getAsJsonPrimitive(propertyName).getAsInt();
    }
    
    public static Double getDoubleProperty(JsonObject jsonObj, String propertyName)
    {
        return jsonObj.getAsJsonPrimitive(propertyName).getAsDouble();
    }

    @SuppressLint("SimpleDateFormat")
    public static Date getDateProperty(JsonObject jsonObj, String propertyName, String format)
    {
        DateFormat df = new SimpleDateFormat(format);
        try
        {
            return df.parse(jsonObj.getAsJsonPrimitive(propertyName).getAsString());
        }
        catch (ParseException e)
        {
            return null;
        }
    }
    
    public static void addNotificationItem(Context context, String text, NotificationItem.Type type)
    {
        addNotificationItem(context, text, type, true);
    }
    
    public static void addNotificationItem(Context context, String text, NotificationItem.Type type, boolean autoSave)
    {
        Globals.notifications.add(new NotificationItem(text, type));
        if (autoSave)
        {
            saveData(context);
        }
    }
    
    @SuppressWarnings("unchecked")
    public static boolean loadData(Context context)
    {
        ObjectInputStream ois = null;
        try
        {
            System.out.println("*** Reading from " + DATA_FILE);
            ois = new ObjectInputStream(context.openFileInput(DATA_FILE));

            Map<WasteContainerType, WasteContainerStats> wcAllStatsHolder = (Map<WasteContainerType, WasteContainerStats>) ois.readObject();
            System.out.println("Read wcAllStats maps size: " + wcAllStatsHolder.size());
            
            String selectedContainerGroupNameHolder = ois.readUTF();
            if ("".equals(selectedContainerGroupNameHolder))
            {
                selectedContainerGroupNameHolder = null;
            }
            System.out.println("Read selectedContainerGroupName: " + selectedContainerGroupNameHolder);
            
            List<NotificationItem> notificationsHolder = (List<NotificationItem>) ois.readObject();
            System.out.println("Read notifications list size: " + notificationsHolder.size());

            Map<String, WasteContainer> wcAllHolder = (Map<String, WasteContainer>) ois.readObject();
            System.out.println("Read wcAll maps size: " + wcAllHolder.size());

            Globals.wcAllStats = wcAllStatsHolder;
            Globals.selectedContainerGroupName = selectedContainerGroupNameHolder;
            Globals.notifications = notificationsHolder;
            Globals.wcAll = wcAllHolder;
            
            System.out.println("*** DONE Reading from " + DATA_FILE);
            return true;
        }
        catch (FileNotFoundException e)
        {
            System.out.println("Data file not found; assuming fresh install...");
        }
        catch (StreamCorruptedException e)
        {
            System.out.println("Stream corrupted exception; assuming fresh install...");
        }
        catch (Exception e)
        {
            System.out.println("Exception; assuming fresh install...");
        }
        finally
        {
            try
            {
                if (ois != null)
                {
                    ois.close();
                }
            }
            catch (IOException e)
            {
                // Ignore
            }
        }
        return false;
    }
    

    public static void saveData(Context context)
    {
        ObjectOutputStream oos = null;
        try
        {
            System.out.println("*** Writing to " + DATA_FILE);
            oos = new ObjectOutputStream(context.openFileOutput(DATA_FILE, Context.MODE_PRIVATE));

            oos.writeObject(Globals.wcAllStats);
            String scgName = Globals.selectedContainerGroupName;
            if (scgName == null)
            {
                scgName = "";
            }
            oos.writeUTF(scgName);
            oos.writeObject(Globals.notifications);
            oos.writeObject(Globals.wcAll);
            System.out.println("*** DONE writing to " + DATA_FILE);
        }
        catch (IOException e)
        {
            System.err.println(e);
        }
        finally
        {
            try
            {
                if (oos != null)
                {
                    oos.close();
                }
            }
            catch (IOException e)
            {
                // Ignore
            }
        }
    }
}
