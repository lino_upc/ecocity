package edu.upc.gessi.ecocity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import edu.upc.gessi.ecocity.model.Globals;
import edu.upc.gessi.ecocity.model.NotificationItem;
import edu.upc.gessi.ecocity.model.WasteContainer;
import edu.upc.gessi.ecocity.model.WasteContainerStats;
import edu.upc.gessi.ecocity.model.WasteContainerType;

public class ComputeStatsTask extends AsyncTask<JsonArray, Void, Void>
{
    private Context context;
    private ComputeStatsTaskCallback callback;
    private ProgressDialog progress;
    private boolean resetStatsAtStart;
    private double pctRecycled;

    public ComputeStatsTask(Context context, boolean resetStatsAtStart)
    {
        this(context, null, resetStatsAtStart);
    }

    public ComputeStatsTask(Context context, ComputeStatsTaskCallback callback, boolean resetStatsAtStart)
    {
        this.context = context;
        this.progress = new ProgressDialog(context);
        this.resetStatsAtStart = resetStatsAtStart;
        this.callback = callback;
    }
    
    @Override
    protected void onPreExecute()
    {
        progress.setMessage(context.getString(R.string.text_calculating_stats));
        progress.show();
    }

    @Override
    protected Void doInBackground(JsonArray... params)
    {
        if (resetStatsAtStart)
        {
            Globals.wcAllStats.clear();
        }
        
        JsonArray jsonArr = params[0];
        int prevReading = 0;
        WasteContainerType prevWCType = null;
        for (JsonElement jsonElm : jsonArr)
        {
            JsonObject jsonWCObj = jsonElm.getAsJsonObject();
            String name = Utils.getStringProperty(jsonWCObj, "deviceName");
            String timestampStr = Utils.getStringProperty(jsonWCObj, "time");
            // Determine the type of this container.
            WasteContainer wc = Globals.wcAll.get(name);
            wc.setLastReading(timestampStr);
            WasteContainerType wcType = wc.getType();
            
            WasteContainerStats wcStats = Globals.wcAllStats.get(wcType);
            if (wcStats == null)
            {
                wcStats = new WasteContainerStats(wcType, wc.getCapacity());
                Globals.wcAllStats.put(wcType, wcStats);
            }
            
            if (prevWCType != wcType)
            {
                // Changing containers, so reset the last reading encountered.
                prevReading = 0;
            }
            
            // Given the sorted list of device data, find "inflection points", i.e.
            // points where the reading went from >0 to 0 (because that means the container was emptied)
            int reading = Utils.getIntegerProperty(jsonWCObj, "value");
            if ((prevReading > 0) && (reading == 0))
            {
                double wasteCollected = ((prevReading/100.0) * wc.getCapacity());
                        
                // Timestamp will be in ISO 8601 format -- capture the part before 
                // the T delimiter so we can use that to show collections per day.
                int delimPos = timestampStr.indexOf('T');
                if (delimPos == -1)
                {
                    // Shouldn't happen, but if it does consider this timestamp invalid.
                    continue;
                }
                String dateStr = timestampStr.substring(0, delimPos);
                
                wcStats.incWasteCollectedOn(dateStr, wasteCollected);
            }
            prevReading = reading;
            prevWCType = wcType;
        }
        
        // Perform stats calculation on each of the container types.
        for (WasteContainerStats wcStats : Globals.wcAllStats.values())
        {
            wcStats.compute();
            System.out.println(wcStats.getType() + ": TOTAL: " + wcStats.getTotalWasteCollected() + 
                               " DAILY AVG: " + wcStats.getDailyAverage() + 
                               " WEEKLY AVG: " + wcStats.getWeeklyAverage());
        }
        
        // Perform stats calculation across all container types.
        computeWasteBreakdown();
        
        // Save computed data.
        Utils.saveData(context);
        
        return null;
    }
    
    private void computeWasteBreakdown()
    {
        // Compute % of recyclable waste (paper, glass, plastic/cans) vs total waste.
        double totalWaste = 0.0;
        double recyclableWaste = 0.0;
        for (WasteContainerStats wcStats : Globals.wcAllStats.values())
        {
            double wcWaste = wcStats.getTotalWasteCollected();
            totalWaste += wcWaste;
            if ((wcStats.getType() == WasteContainerType.PAPER) ||
                (wcStats.getType() == WasteContainerType.PLASTIC) ||
                (wcStats.getType() == WasteContainerType.GLASS))
            {
                recyclableWaste += wcWaste;
            }
        }
        
        pctRecycled = (recyclableWaste / totalWaste);
    }

    @Override
    protected void onPostExecute(Void result)
    {
        progress.dismiss();
        
        if (pctRecycled > 0.5)
        {
            String text = context.getString(R.string.text_hood_recycle_info, Globals.DECI_FORMAT.format(pctRecycled * 100));
            Utils.longToast(context, text);
            Utils.addNotificationItem(context, text, 
                                      NotificationItem.Type.RECYCLING_STATS);
        }
        
        if (callback != null)
        {
            callback.onComputeStatsSuccess();
        }
    }
    
    public static interface ComputeStatsTaskCallback
    {
        void onComputeStatsSuccess();
    }
}