package edu.upc.gessi.ecocity.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

public abstract class HTTPRespJSONCallback implements HTTPRespCallback<JsonElement>
{
    @Override
    public JsonElement processResponseString(String response)
    {
        Gson gson = new GsonBuilder().create();
        return preprocessJsonElement(gson.fromJson(response, JsonElement.class));
    }
    
    // Subclasses can override if they wish to further process the resulting
    // JsonElement while still in the background thread.
    protected JsonElement preprocessJsonElement(JsonElement jsonElm)
    {
        return jsonElm;
    }
}
