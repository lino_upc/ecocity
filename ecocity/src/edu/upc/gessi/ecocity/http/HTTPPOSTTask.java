package edu.upc.gessi.ecocity.http;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;

public class HTTPPOSTTask<T> extends HTTPReqTask<T>
{
    @Override
    protected void configureConnection(HttpURLConnection conn, HTTPReqTaskParam<T> param) throws IOException
    {
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded ");
        
        String paramString = param.getParamString();
        System.out.println("POST data: " + paramString);
        byte[] body = paramString.getBytes();
        conn.setFixedLengthStreamingMode(body.length);
        conn.setDoOutput(true);
 
        OutputStream out = conn.getOutputStream();
        out.write(body);
    }
    
    @SuppressWarnings("unchecked")
    public static <T> void exec(HTTPReqTaskParam<T> params)
    {
        new HTTPPOSTTask<T>().execute(params);
    }
}