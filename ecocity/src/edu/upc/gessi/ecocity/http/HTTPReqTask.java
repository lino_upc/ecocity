package edu.upc.gessi.ecocity.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.os.AsyncTask;

public abstract class HTTPReqTask<T> extends AsyncTask<HTTPReqTaskParam<T>, Void, T>
{
    private int httpRespCode;
    private String rawResponse;
    private Exception reqException;
    private HTTPRespCallback<T> callback;
    private Activity activity;

    @SuppressWarnings("unchecked")
    @Override
    protected T doInBackground(HTTPReqTaskParam<T>... params)
    {
        // We only work with one request at a time.
        HTTPReqTaskParam<T> param = params[0];
        
        callback = param.getCallback();
        activity = param.getActivity();
        
        rawResponse = execHTTPRequest(param);
        if (rawResponse != null)
        {
            try
            {
                return callback.processResponseString(rawResponse);
            }
            catch (Exception e)
            {
                reqException = e;
            }
        }
        return null;
    }
    
    protected URL getURL(HTTPReqTaskParam<T> param) throws MalformedURLException
    {
        return new URL(param.getURLString());
    }

    protected abstract void configureConnection(HttpURLConnection conn, HTTPReqTaskParam<T> param) throws IOException;
    
    @Override
    protected void onPostExecute(T response)
    {
        if (callback != null)
        {
            if (isHTTPSuccess() && (reqException == null))
            {
                callback.onSuccess(activity, response);
            }
            else
            {
                callback.onError(activity, rawResponse, reqException);
            }
        }
    }
    
    private boolean isHTTPSuccess()
    {
        return httpRespCode >= 200 && httpRespCode <= 300;
    }
    
    protected int getHTTPResponseCode()
    {
        return httpRespCode;
    }
    
    private String execHTTPRequest(HTTPReqTaskParam<T> param)
    {
        HttpURLConnection conn = null;
        BufferedReader rd = null;
        try
        {
            URL url = getURL(param);
            conn = (HttpURLConnection)url.openConnection();
            configureConnection(conn, param);
            
            httpRespCode = conn.getResponseCode();
            System.out.println("Response code: " + httpRespCode);
            if (isHTTPSuccess()) 
            {
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } 
            else 
            {
                rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            }
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) 
            {
                sb.append(line);
            }
            return sb.toString();
        }
        catch (Exception e)
        {
            reqException = e;
        }
        finally
        {
            if (rd != null)
            {
                try
                {
                    rd.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            if (conn != null)
            {
                conn.disconnect();
            }
        }
        return null;
    }
}