package edu.upc.gessi.ecocity.http;

import android.app.Activity;

public interface HTTPRespCallback<T>
{
    void onSuccess(Activity activity, T response);
    void onError(Activity activity, String response, Exception e);
    T processResponseString(String response) throws Exception;
}
