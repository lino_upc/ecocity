package edu.upc.gessi.ecocity.http;

import java.io.StringReader;

import org.xmlpull.v1.XmlPullParser;

import android.util.Xml;

public abstract class HTTPRespXMLCallback implements HTTPRespCallback<XmlPullParser>
{
    @Override
    public XmlPullParser processResponseString(String response) throws Exception
    {
        XmlPullParser parser = Xml.newPullParser();
        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(new StringReader(response));
        parser.nextTag();
        return parser;
    }

    protected String readText(XmlPullParser parser) throws Exception
    {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) 
        {
            result = parser.getText();
            parser.nextTag();
        }
        return result;    
    }
    
    protected void skip(XmlPullParser parser) throws Exception 
    {
        if (parser.getEventType() != XmlPullParser.START_TAG) 
        {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) 
        {
            switch (parser.next()) 
            {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
     }}
