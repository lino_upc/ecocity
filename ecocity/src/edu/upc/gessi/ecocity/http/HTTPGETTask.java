package edu.upc.gessi.ecocity.http;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HTTPGETTask<T> extends HTTPReqTask<T>
{
    @Override
    protected URL getURL(HTTPReqTaskParam<T> param) throws MalformedURLException
    {
        StringBuilder sb = new StringBuilder(param.getURLString());
        if (param.hasParams())
        {
            sb.append("?").append(param.getParamString());
        }
        System.out.println("HTTP GET URL: " + sb.toString());
        return new URL(sb.toString());
    }

    @Override
    protected void configureConnection(HttpURLConnection conn,
                                       HTTPReqTaskParam<T> param) throws IOException
    {
        conn.setRequestMethod("GET");
    }
    
    @SuppressWarnings("unchecked")
    public static <T> void exec(HTTPReqTaskParam<T> params)
    {
        new HTTPGETTask<T>().execute(params);
    }
}