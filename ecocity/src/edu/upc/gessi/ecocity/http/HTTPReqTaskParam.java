package edu.upc.gessi.ecocity.http;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;

public class HTTPReqTaskParam<T>
{
    private String urlString;
    private Map<String,String> params;
    private Activity activity;
    private HTTPRespCallback<T> callback;
    
    public HTTPReqTaskParam(Activity activity, String urlString, HTTPRespCallback<T> callback)
    {
        this.activity = activity;
        this.urlString = urlString;
        this.callback = callback;
        params = new HashMap<String,String>();
    }
    
    public String getURLString()
    {
        return urlString;
    }
    
    public Activity getActivity()
    {
        return activity;
    }
    
    public HTTPRespCallback<T> getCallback()
    {
        return callback;
    }
    
    public void addParam(String name, String value)
    {
        try
        {
            params.put(name, URLEncoder.encode(value, "UTF-8"));
        }
        catch (UnsupportedEncodingException e)
        {
            // Ignore; should never happen.
        }
    }
    
    public boolean hasParams()
    {
        return (params.size() > 0);
    }

    public String getParamString()
    {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet())
        {
            if (sb.length() > 0)
            {
                sb.append("&");
            }
            sb.append(entry.getKey()).append("=").append(entry.getValue());
        }
        return sb.toString();
    }
}