package edu.upc.gessi.ecocity.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import edu.upc.gessi.ecocity.R;
import edu.upc.gessi.ecocity.model.Globals;
import edu.upc.gessi.ecocity.model.NotificationItem;

/**
 * The simplest possible example of using AndroidPlot to plot some data.
 */
public class NotificationActivity extends Activity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        
        ListView listview = (ListView) findViewById(R.id.listView_notifications);
        // We display the notifications with the newest (i.e. ones that were added later)
        // on top.
        List<NotificationItem> reversedList = new ArrayList<NotificationItem>(Globals.notifications);
        Collections.reverse(reversedList);
        NotificationItemAdapter adapter = new NotificationItemAdapter(this, reversedList.toArray(new NotificationItem[reversedList.size()]));
        listview.setAdapter(adapter);    
    }
    
    @SuppressLint("SimpleDateFormat")
    private static class NotificationItemAdapter extends ArrayAdapter<NotificationItem> 
    {
        private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat();

        public NotificationItemAdapter(Context context, NotificationItem[] items) 
        {
            super(context, R.layout.notification_item_layout, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) 
        {
            if (convertView == null)
            {
                LayoutInflater i = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = (View) i.inflate(R.layout.notification_item_layout, parent, false);
            }
            TextView textViewInfo = (TextView)convertView.findViewById(R.id.textView_notifitem_info);
            TextView textViewTimestamp = (TextView)convertView.findViewById(R.id.textView_notifitem_timestamp);
            ImageView imageViewIcon = (ImageView) convertView.findViewById(R.id.imageView_notifitem_icon);
            NotificationItem item = super.getItem(position);
            textViewInfo.setText(item.getText());
            textViewTimestamp.setText(getContext().getString(R.string.text_sent, DATE_FORMAT.format(new Date(item.getTimestamp()))));

            int iconId = R.drawable.ic_launcher;
            switch (item.getType())
            {
                case GENERAL_INFO:
                    iconId = R.drawable.ic_launcher;
                    break;
                case PROBLEM_REPORTED:
                    iconId = R.drawable.ic_launcher_report;
                    break;
                case RECYCLING_STATS:
                    iconId = R.drawable.ic_recycle;
                    break;
                default:
                    break;
            }

            imageViewIcon.setImageResource(iconId);
            return convertView;
        }      
    }
}
