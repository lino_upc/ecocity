package edu.upc.gessi.ecocity.activity;

import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import edu.upc.gessi.ecocity.R;
import edu.upc.gessi.ecocity.Utils;
import edu.upc.gessi.ecocity.http.HTTPPOSTTask;
import edu.upc.gessi.ecocity.http.HTTPReqTaskParam;
import edu.upc.gessi.ecocity.http.HTTPRespXMLCallback;
import edu.upc.gessi.ecocity.model.Globals;
import edu.upc.gessi.ecocity.model.NotificationItem.Type;
import edu.upc.gessi.ecocity.model.WasteContainer;
import edu.upc.gessi.ecocity.model.WasteContainerType;

public class ReportProblemActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_problem);
        
        // Create Container Types spinner.
        final Spinner contType = Utils.setSpinnerData(this, R.id.spinner_container_type, 
                                             R.array.spinner_container_types);
        // Create pre-defined Problems spinner.
        final Spinner problem = Utils.setSpinnerData(this, R.id.spinner_report_problem, 
                                            R.array.spinner_problems);
 
        Button button = (Button) findViewById(R.id.button_report_problem);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onReportProblem(contType, problem);
            }
        });    
    }

    protected void onReportProblem(Spinner contTypeSpinner, Spinner problemSpinner)
    {
        // Get the data that the user supplied about the problem.
        String contType = contTypeSpinner.getSelectedItem().toString();
        List<WasteContainer> wcs = getContainersOfType(contTypeSpinner.getSelectedItemPosition());
        StringBuilder sb = new StringBuilder();
        for (WasteContainer wc : wcs)
        {
            if (sb.length() > 0)
            {
                sb.append(",");
            }
            sb.append(wc.getName());
        }
        String wcNames = sb.toString();
        String problem = problemSpinner.getSelectedItem().toString();
        EditText descriptionEditText = (EditText)findViewById(R.id.edittext_problem_description);
        String description = problem + ": " + descriptionEditText.getText().toString() + "\n(Container: " + contType + ", Device Name(s): " + wcNames + ")";

        // Create the HTTP POST request to the iCity IRIS API.
        String urlString = "http://icity-gw.icityproject.com:8080/api/requests";
        HTTPReqTaskParam<XmlPullParser> taskParams = new HTTPReqTaskParam<XmlPullParser>(this, urlString, new HTTPRespXMLCallback() {
            
            @Override
            public void onSuccess(Activity activity, XmlPullParser parser)
            {
                try
                {
                    String reqId = "";
                    // Require root element named service_requests.
                    parser.require(XmlPullParser.START_TAG, null, "service_requests");
                    while (parser.next() != XmlPullParser.END_TAG) 
                    {
                        if (parser.getEventType() != XmlPullParser.START_TAG) 
                        {
                            continue;
                        }
                        // Look for the service request id token.
                        String name = parser.getName();
                        // Starts by looking for the entry tag
                        if (name.equals("request")) 
                        {
                            parser.require(XmlPullParser.START_TAG, null, "request");
                            while (parser.next() != XmlPullParser.END_TAG) 
                            {
                                if (parser.getEventType() != XmlPullParser.START_TAG) 
                                {
                                    continue;
                                }
                                name = parser.getName();
                                if (name.equals("service_request_id")) 
                                {
                                    parser.require(XmlPullParser.START_TAG, null, "service_request_id");
                                    reqId = readText(parser);
                                    parser.require(XmlPullParser.END_TAG, null, "service_request_id");
                                    Utils.addNotificationItem(ReportProblemActivity.this, 
                                                              activity.getString(R.string.text_thanks_for_reporting, reqId), 
                                                              Type.PROBLEM_REPORTED);
                                } 
                                else 
                                {
                                    skip(parser);
                                }
                            }
                        } 
                        else 
                        {
                            skip(parser);
                        }
                    }
                    Utils.longToast(ReportProblemActivity.this, activity.getString(R.string.text_problem_reported, reqId));
                }
                catch (Exception e)
                {
                    onError(ReportProblemActivity.this, null, e);
                }
            }
            
            @Override
            public void onError(Activity activity, String response, Exception e)
            {
                if (e != null)
                {
                    e.printStackTrace();
                }
                Utils.shortToast(ReportProblemActivity.this, R.string.text_error);
            }
        });
        
        taskParams.addParam("apikey", Globals.ICITY_311_API_KEY);
        taskParams.addParam("description", description);
        taskParams.addParam("jurisdiction_id", "1");
        taskParams.addParam("service_code", "001");
        String lat = "0", longi = "0";
        if (wcs.size() > 0)
        {
            lat = Double.toString(wcs.get(0).getLatitude());
            longi = Double.toString(wcs.get(0).getLongitude());
        }
        taskParams.addParam("lat", lat);
        taskParams.addParam("long", longi);
        
        HTTPPOSTTask.exec(taskParams);
        finish();
    }

    private List<WasteContainer> getContainersOfType(int contTypeIdx)
    {
        // Find containers of the given type.
        // The spinner index corresponds to the declaration order of the enum.
        List<WasteContainer> wcsOfType = new ArrayList<WasteContainer>();
        WasteContainerType type = WasteContainerType.values()[contTypeIdx];
        for (WasteContainer wc : Globals.wcAll.values())
        {
            if (wc.getType() == type)
            {
                wcsOfType.add(wc);
            }
        }
        
        return wcsOfType;
    }
}
