package edu.upc.gessi.ecocity.activity;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.androidplot.LineRegion;
import com.androidplot.ui.AnchorPosition;
import com.androidplot.ui.SeriesRenderer;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.ui.TextOrientationType;
import com.androidplot.ui.XLayoutStyle;
import com.androidplot.ui.YLayoutStyle;
import com.androidplot.ui.widget.TextLabelWidget;
import com.androidplot.util.PixelUtils;
import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BarRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.SimpleXYSeries.ArrayFormat;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.XYStepMode;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import edu.upc.gessi.ecocity.ComputeStatsTask;
import edu.upc.gessi.ecocity.R;
import edu.upc.gessi.ecocity.Utils;
import edu.upc.gessi.ecocity.ComputeStatsTask.ComputeStatsTaskCallback;
import edu.upc.gessi.ecocity.http.HTTPGETTask;
import edu.upc.gessi.ecocity.http.HTTPReqTaskParam;
import edu.upc.gessi.ecocity.http.HTTPRespJSONCallback;
import edu.upc.gessi.ecocity.model.Globals;
import edu.upc.gessi.ecocity.model.WasteContainer;
import edu.upc.gessi.ecocity.model.WasteContainerStats;
import edu.upc.gessi.ecocity.model.WasteContainerType;

@SuppressLint("SimpleDateFormat")
public class StatsActivity extends Activity
{
    private static final DecimalFormat DECI_FORMAT_WHOLE_NUM = new DecimalFormat("0");
    private static final DateFormat DAY_SLASH_MONTH = new SimpleDateFormat("dd/MM");
    private static final int MILLIS_IN_DAY = 24 * 60 * 60 * 1000;
    private static final DateFormat YEAR_DASH_MONTH_DASH_DAY = new SimpleDateFormat("yyyy-MM-dd");

    private XYPlot allContainersPlot;
    private XYPlot dailyStatsPlot;

    // Barplot objects
    private MyBarFormatter barFormatter;
    private MyBarFormatter highlightedBarFormatter;
    private Pair<Integer, XYSeries> selection = null;
    private TextLabelWidget infoWidget;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
        
        configureAllContainersPlotObjects();
        configureDailyStatsPlotObjects();

        configureSpinner();
        
        Button getLatestButton = (Button)findViewById(R.id.button_get_latest_data);
        getLatestButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                getLatestData();
            }
        });
    }

    private void configureSpinner()
    {
        Spinner statTypeSpinner = Utils.setSpinnerData(this, R.id.spinner_stat_type, 
                                                       R.array.spinner_stat_types);
        statTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if ((position == 0) || (position == 1))
                {
                    // Show daily/weekly average stats for all containers.
                    infoWidget.setText(getString(R.string.prompt_touch_bar));
                    showAllContainersBarChart((position == 0));
                }
                else
                {
                    // Show individual container stats.
                    showContainerStats(WasteContainerType.values()[position - 2]);
                }
            }
        
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {
            // Do nothing.
            }
        });
    }
    
    private void configureDailyStatsPlotObjects()
    {
        dailyStatsPlot = (XYPlot)findViewById(R.id.aplot_daily_stats);
        dailyStatsPlot.setTicksPerDomainLabel(2);
        // TODO better to use a custom formatter?
        dailyStatsPlot.getGraphWidget().setDomainLabelOrientation(-45);
        dailyStatsPlot.setDomainValueFormat(DAY_SLASH_MONTH);
        
        dailyStatsPlot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, 1000.0);
        dailyStatsPlot.setTicksPerRangeLabel(2);
        dailyStatsPlot.setRangeBoundaries(0, 3500, BoundaryMode.FIXED);
        dailyStatsPlot.setRangeValueFormat(DECI_FORMAT_WHOLE_NUM);
        
        dailyStatsPlot.getLegendWidget().setVisible(false);
    }

    @SuppressWarnings("serial")
    private void configureAllContainersPlotObjects()
    {
        allContainersPlot = (XYPlot)findViewById(R.id.aplot_all_containers);
        allContainersPlot.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) 
            {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) 
                {
                    onPlotClicked(new PointF(motionEvent.getX(), motionEvent.getY()));
                }
                return true;
            }
        });
        allContainersPlot.setDomainValueFormat(new NumberFormat() {
            @Override
            public Number parse(String string, ParsePosition position)
            {
                return 0;
            }
            
            @Override
            public StringBuffer format(long value, StringBuffer buffer, FieldPosition field)
            {
                return buffer;
            }
            
            @Override
            public StringBuffer format(double value, StringBuffer buffer, FieldPosition field)
            {
                WasteContainerType[] wcTypes = WasteContainerType.values();
                if ((value < 0) || (value > wcTypes.length))
                {
                    return buffer;
                }
                WasteContainerType wcType = wcTypes[(int)value];
                if (wcType != null)
                {
                    int labelResId = getWasteStringResId(wcType);
                    return new StringBuffer(getString(labelResId));
                }
                return buffer;
            }
        });
        allContainersPlot.setDomainStep(XYStepMode.INCREMENT_BY_VAL, 1.0);
        
        allContainersPlot.setRangeLowerBoundary(0, BoundaryMode.FIXED);
        allContainersPlot.setRangeValueFormat(DECI_FORMAT_WHOLE_NUM);
        allContainersPlot.setTicksPerRangeLabel(2);
        
        allContainersPlot.getGraphWidget().setGridPadding(30, 10, 30, 0);
        allContainersPlot.getLegendWidget().setVisible(false);
        
        // Configure info widget.
        infoWidget = new TextLabelWidget(allContainersPlot.getLayoutManager(), "",
                                         new SizeMetrics(PixelUtils.dpToPix(100), SizeLayoutType.ABSOLUTE,
                                         PixelUtils.dpToPix(100), SizeLayoutType.ABSOLUTE),
                                         TextOrientationType.HORIZONTAL);

        infoWidget.getLabelPaint().setTextSize(PixelUtils.dpToPix(16));

        // Add a dark, semi-transparent background to the selection label widget.
        Paint p = new Paint();
        p.setARGB(100, 0, 0, 0);
        infoWidget.setBackgroundPaint(p);

        infoWidget.position(0, XLayoutStyle.RELATIVE_TO_CENTER,
                            PixelUtils.dpToPix(45), 
                            YLayoutStyle.ABSOLUTE_FROM_TOP,
                            AnchorPosition.TOP_MIDDLE);
        infoWidget.pack();
        
        // Instantiate formatters.
        barFormatter = new MyBarFormatter(Color.argb(200, 100, 150, 100), Color.LTGRAY);
        highlightedBarFormatter = new MyBarFormatter(Color.YELLOW, Color.WHITE);
    }

    private int getWasteStringResId(WasteContainerType wcType) 
    {
        int labelResId = R.string.text_waste_type_waste;
        switch (wcType) 
        {
            case WASTE: labelResId = R.string.text_waste_type_waste; break;
            case PAPER: labelResId = R.string.text_waste_type_paper; break;
            case GLASS: labelResId = R.string.text_waste_type_glass; break;
            case PLASTIC: labelResId = R.string.text_waste_type_plastic; break;
            case ORGANIC: labelResId = R.string.text_waste_type_organic; break;
        
        }
        return labelResId;
    }
    
    private void getLatestData()
    {
        progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.text_obtaining_readings));
        progress.show();        
        
        String urlString = "http://icity-gw.icityproject.com:8080/developer/api/observations/last";
        HTTPReqTaskParam<JsonElement> taskParam = new HTTPReqTaskParam<JsonElement>(this, urlString, new GetLatestDataCallback());
        
        taskParam.addParam("apikey", Globals.ICITY_API_KEY);
        taskParam.addParam("infrastructureid", "20");
        taskParam.addParam("property", "urn:container_volum");
        taskParam.addParam("n", "20");          // Get last 20 readings for device
        HTTPGETTask.exec(taskParam);
    }
    
    protected void onPlotClicked(PointF point)
    {
        if (allContainersPlot.getGraphWidget().getGridRect().contains(point.x, point.y)) 
        {
            Number x = allContainersPlot.getXVal(point);
            Number y = allContainersPlot.getYVal(point);

            selection = null;
            double xDistance = 0;
            double yDistance = 0;

            // find the closest value to the selection:
            for (XYSeries series : allContainersPlot.getSeriesSet()) 
            {
                for (int i = 0; i < series.size(); i++) 
                {
                    Number thisX = series.getX(i);
                    Number thisY = series.getY(i);
                    if (thisX != null && thisY != null) 
                    {
                        double thisXDistance = LineRegion.measure(x, thisX).doubleValue();
                        double thisYDistance = LineRegion.measure(y, thisY).doubleValue();
                        if (selection == null) 
                        {
                            selection = new Pair<Integer, XYSeries>(i, series);
                            xDistance = thisXDistance;
                            yDistance = thisYDistance;
                        } 
                        else if (thisXDistance < xDistance) 
                        {
                            selection = new Pair<Integer, XYSeries>(i, series);
                            xDistance = thisXDistance;
                            yDistance = thisYDistance;
                        } 
                        else if (thisXDistance == xDistance &&
                                 thisYDistance < yDistance &&
                                 thisY.doubleValue() >= y.doubleValue()) 
                        {
                            selection = new Pair<Integer, XYSeries>(i, series);
                            xDistance = thisXDistance;
                            yDistance = thisYDistance;
                        }
                    }
                }
            }
        } 
        else 
        {
            // If the press was outside the graph area, hide info popup.
            selection = null;
        }

        if (selection == null) 
        {
            infoWidget.setText(getString(R.string.prompt_touch_bar));
        } 
        else 
        {
            double value = selection.second.getY(selection.first).doubleValue();
            String type = getString(getWasteStringResId(WasteContainerType.values()[selection.first]));
            infoWidget.setText(type + ": " + Globals.DECI_FORMAT.format(value) + "L");
        }
        
        allContainersPlot.redraw();
    }

    private void showContainerStats(WasteContainerType wcType)
    {
        WasteContainerStats wcStats = Globals.wcAllStats.get(wcType);
        if (wcStats == null)
        {
            System.err.println("No stats found for " + wcType);
            return;
        }
        System.out.println("Showing stats for " + wcType);
        
        allContainersPlot.setVisibility(View.INVISIBLE);
        
        findViewById(R.id.textView_capacity_label).setVisibility(View.VISIBLE);
        Utils.setStaticText(this, R.id.textView_capacity, Globals.DECI_FORMAT.format(wcStats.getCapacity()) + "L");
        findViewById(R.id.textView_daily_avg_label).setVisibility(View.VISIBLE);
        Utils.setStaticText(this, R.id.textView_daily_avg, Globals.DECI_FORMAT.format(wcStats.getDailyAverage()) + "L");
        findViewById(R.id.textView_weekly_avg_label).setVisibility(View.VISIBLE);
        Utils.setStaticText(this, R.id.textView_weekly_avg, Globals.DECI_FORMAT.format(wcStats.getWeeklyAverage()) + "L");
        
        dailyStatsPlot.clear();
        
        // Construct a parallel list of timestamps and waste collected on that day.
        List<Long> timestamps = new ArrayList<Long>();
        List<Double> wasteCollected = new ArrayList<Double>();
        for (Map.Entry<String, Double> e : wcStats.getWasteCollectedByDay().entrySet())
        {
            try
            {
                timestamps.add(YEAR_DASH_MONTH_DASH_DAY.parse(e.getKey()).getTime());
                wasteCollected.add(e.getValue());
            }
            catch (ParseException e1)
            {
                // Ignore; should never happen
            }
        }
        
        // Just show the last 5 values.
        if (timestamps.size() > 5)
        {
            timestamps = timestamps.subList(timestamps.size() - 5, timestamps.size());
        }
        if (wasteCollected.size() > 5)
        {
            wasteCollected = wasteCollected.subList(wasteCollected.size() - 5, wasteCollected.size());
        }
        
        // Turn the above arrays into XYSeries':
        XYSeries data = new SimpleXYSeries(timestamps, wasteCollected, "");
 
        // Create a formatter to use for drawing a series using LineAndPointRenderer
        // and configure it from xml:
        LineAndPointFormatter series1Format = new LineAndPointFormatter();
        series1Format.setPointLabelFormatter(new PointLabelFormatter());
        series1Format.configure(getApplicationContext(), R.xml.line_point_formatter_with_plf1);
 
        dailyStatsPlot.addSeries(data, series1Format);
        dailyStatsPlot.setDomainBoundaries(timestamps.get(0) - MILLIS_IN_DAY, 
                                 timestamps.get(timestamps.size() - 1) + MILLIS_IN_DAY, 
                                 BoundaryMode.FIXED);
        dailyStatsPlot.setVisibility(View.VISIBLE);
        dailyStatsPlot.redraw();
    }
    
    private void showAllContainersBarChart(boolean dailyAverage)
    {
        // Hide the daily stats plot view objects.
        findViewById(R.id.textView_capacity_label).setVisibility(View.INVISIBLE);
        findViewById(R.id.textView_capacity).setVisibility(View.INVISIBLE);
        findViewById(R.id.textView_daily_avg_label).setVisibility(View.INVISIBLE);
        findViewById(R.id.textView_daily_avg).setVisibility(View.INVISIBLE);
        findViewById(R.id.textView_weekly_avg_label).setVisibility(View.INVISIBLE);
        findViewById(R.id.textView_weekly_avg).setVisibility(View.INVISIBLE);
        dailyStatsPlot.setVisibility(View.INVISIBLE);
        
        allContainersPlot.clear();
        allContainersPlot.setRangeStep(XYStepMode.INCREMENT_BY_VAL, dailyAverage ? 250.0 : 1500);
        allContainersPlot.setRangeUpperBoundary(dailyAverage ? 2500 : 12500, BoundaryMode.FIXED);
        
        List<Double> wcAverages = new ArrayList<Double>();
        final WasteContainerType[] wcTypes = WasteContainerType.values();
        for (WasteContainerType wcType : wcTypes)
        {
            WasteContainerStats wcs = Globals.wcAllStats.get(wcType);
            if (wcs == null)
            {
                continue;
            }
            
            double average = dailyAverage ? wcs.getDailyAverage() : wcs.getWeeklyAverage();
            wcAverages.add(average);
            System.out.println(wcs.getType() + ": TOTAL: " + wcs.getTotalWasteCollected() + " AVG: " + average);
        }
        // Create the XYSeries that represents our data, and put it on our plot.
        XYSeries data = new SimpleXYSeries(wcAverages, ArrayFormat.Y_VALS_ONLY, "");
        allContainersPlot.addSeries(data, barFormatter);
        allContainersPlot.setVisibility(View.VISIBLE);
        allContainersPlot.redraw();
    }
    
    
    private class GetLatestDataCallback extends HTTPRespJSONCallback implements ComputeStatsTaskCallback
    {
        @Override
        protected JsonElement preprocessJsonElement(JsonElement jsonElm)
        {
            JsonArray jsonArr = jsonElm.getAsJsonArray();
//                System.out.println("Got " + jsonArr.size() + " elements");
            JsonArray resultArr = new JsonArray();
            // Need to filter and reorder those readings that we are interested in.
            for (JsonElement jsonElm2 : jsonArr)
            {
                JsonObject jsonObj = jsonElm2.getAsJsonObject();
                String name = Utils.getStringProperty(jsonObj, "deviceName");
                String timestampStr = Utils.getStringProperty(jsonObj, "time");
                WasteContainer wc = Globals.wcAll.get(name);
                if ((wc != null) && (wc.isLastReadingEarlierThan(timestampStr)))
                {
                    resultArr.add(jsonObj);
                }
            }
            
            // Now reverse the resulting JSON array to produce one with ascending timestamps.
            for (int i = 0, j = resultArr.size() - 1; i < j; i++, j--)
            {
                if (i >= j)
                {
                    break;
                }
                // Swap elements at either end of the array.
                JsonElement temp = resultArr.get(i);
                resultArr.set(i, resultArr.get(j));
                resultArr.set(j, temp);
            }
            
            return resultArr;
        }

        @Override
        public void onError(Activity activity, String response, Exception e)
        {
            progress.dismiss();
            if (e != null)
            {
                e.printStackTrace();
            }
            Utils.shortToast(StatsActivity.this, R.string.text_error);
        }

        @Override
        public void onSuccess(Activity activity, JsonElement jsonElm)
        {
            JsonArray resultArr = jsonElm.getAsJsonArray();
            System.out.println("Final array " + resultArr.size() + " elements");
            progress.dismiss();
            if (resultArr.size() > 0)
            {
                new ComputeStatsTask(StatsActivity.this, this, false).execute(resultArr);
            }
        }

        @Override
        public void onComputeStatsSuccess()
        {
            // Refresh graph, depending on current spinner choice.
            Spinner statsTypeSpinner = (Spinner)StatsActivity.this.findViewById(R.id.spinner_stat_type);
            int selectedItemPos = statsTypeSpinner.getSelectedItemPosition();
            if ((selectedItemPos == 0) || (selectedItemPos == 1))
            {
                // Show daily/weekly average stats for all containers.
                infoWidget.setText(getString(R.string.prompt_touch_bar));
                showAllContainersBarChart((selectedItemPos == 0));
            }
            else
            {
                // Show individual container stats.
                showContainerStats(WasteContainerType.values()[selectedItemPos - 2]);
            }
        }
    }

    @SuppressWarnings("rawtypes")
    class MyBarFormatter extends BarFormatter 
    {
        public MyBarFormatter(int fillColor, int borderColor) 
        {
            super(fillColor, borderColor);
        }

        @Override
        public Class<? extends SeriesRenderer> getRendererClass() 
        {
            return MyBarRenderer.class;
        }

        @Override
        public SeriesRenderer getRendererInstance(XYPlot plot) 
        {
            return new MyBarRenderer(plot);
        }
    }

    class MyBarRenderer extends BarRenderer<MyBarFormatter> 
    {
        public MyBarRenderer(XYPlot plot) 
        {
            super(plot);
            setBarRenderStyle(BarRenderer.BarRenderStyle.SIDE_BY_SIDE);
            setBarWidthStyle(BarRenderer.BarWidthStyle.FIXED_WIDTH);
            setBarWidth(35);
        }

        /**
         * Implementing this method to allow us to inject our
         * special selection formatter.
         * @param index index of the point being rendered.
         * @param series XYSeries to which the point being rendered belongs.
         * @return
         */
        @Override
        public MyBarFormatter getFormatter(int index, XYSeries series) 
        { 
            if (selection != null && selection.second == series && selection.first == index) 
            {
                return highlightedBarFormatter;
            } 
            else 
            {
                return getFormatter(series);
            }
        }
    }
}
