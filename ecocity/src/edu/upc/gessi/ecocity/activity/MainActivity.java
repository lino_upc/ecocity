package edu.upc.gessi.ecocity.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import edu.upc.gessi.ecocity.ComputeStatsTask;
import edu.upc.gessi.ecocity.LabelledImage;
import edu.upc.gessi.ecocity.MainScreenImageAdapter;
import edu.upc.gessi.ecocity.R;
import edu.upc.gessi.ecocity.Utils;
import edu.upc.gessi.ecocity.http.HTTPGETTask;
import edu.upc.gessi.ecocity.http.HTTPReqTaskParam;
import edu.upc.gessi.ecocity.http.HTTPRespJSONCallback;
import edu.upc.gessi.ecocity.model.Globals;
import edu.upc.gessi.ecocity.model.NotificationItem.Type;
import edu.upc.gessi.ecocity.model.WasteContainer;
import edu.upc.gessi.ecocity.model.WasteContainerType;

public class MainActivity extends Activity
{
    private LabelledImage[] mainScreenImages = {
            new LabelledImage(R.drawable.launcher_stats, R.string.title_activity_stats),
            new LabelledImage(R.drawable.launcher_notifications, R.string.title_activity_notification),
            new LabelledImage(R.drawable.launcher_report, R.string.title_activity_report_problem),
            new LabelledImage(R.drawable.launcher_settings, R.string.title_activity_settings)
    };
    private ProgressDialog progress;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        GridView gridview = (GridView) findViewById(R.id.gridview);
        gridview.setAdapter(new MainScreenImageAdapter(this, mainScreenImages));
        gridview.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                nextScreen(position);
            }
        });
        
        doInitialConfig();
    }

    private void doInitialConfig()
    {
        // Do we already have data in storage?
        if (Utils.loadData(this))
        {
            // If yes, do nothing, maybe fetch updated data
            if ((Globals.selectedContainerGroupName != null) && (Globals.wcAllStats.isEmpty()))
            {
                // We know the group but no data/stats, so fetch them now.
                fetchContainerList();
            }
        }
        else
        {
            // If no, do we already have the user's choice of container group?
            if (Globals.selectedContainerGroupName != null)
            {
                // If yes, fetch data for this group.
                fetchContainerList();
            }
            else
            {
                Utils.addNotificationItem(this, getString(R.string.text_app_welcome), Type.GENERAL_INFO);
                // Prompt the user to select a container group.
                nextScreen(3);
            }
        }
    }

    private void fetchContainerList()
    {
        progress = new ProgressDialog(this);
        progress.setMessage(this.getString(R.string.text_obtaining_data));
        progress.show();        
        String urlString = "http://silver-spring.appspot.com/containers/groups/full";
        HTTPReqTaskParam<JsonElement> taskParam = new HTTPReqTaskParam<JsonElement>(this, urlString, new HTTPRespJSONCallback() {

            @Override
            public void onError(Activity activity, String response, Exception e)
            {
                if (e != null)
                {
                    e.printStackTrace();
                }
                Utils.shortToast(MainActivity.this, R.string.text_error);
            }
            
            @Override
            public void onSuccess(Activity activity, JsonElement jsonElm)
            {
                // Build a list of device names.
                JsonArray jsonContainers = jsonElm.getAsJsonObject().get("containers").getAsJsonArray();
                Globals.wcAll.clear();
                for (JsonElement e : jsonContainers)
                {
                    JsonObject jsonObj = e.getAsJsonObject();
                    String name = Utils.getStringProperty(jsonObj, "name");
                    WasteContainerType type = WasteContainerType.valueOf(Utils.getStringProperty(jsonObj, "type"));
                    int capacity = Utils.getIntegerProperty(jsonObj, "capacity");
                    double latitude = Utils.getDoubleProperty(jsonObj, "latitude");
                    double longitude = Utils.getDoubleProperty(jsonObj, "longitude");
                    Globals.wcAll.put(name, new WasteContainer(name, type, capacity, latitude, longitude));
                }
                
                fetchContainerReadings();
            }
        });
        
        taskParam.addParam("name", Globals.selectedContainerGroupName);
        HTTPGETTask.exec(taskParam);
    }
    
    private void fetchContainerReadings()
    {
        progress.setMessage(this.getString(R.string.text_obtaining_readings));
        String urlString = "http://silver-spring.appspot.com/containers/readings";
        HTTPReqTaskParam<JsonElement> reqTaskParam = new HTTPReqTaskParam<JsonElement>(this, urlString, new HTTPRespJSONCallback() {
            
            @Override
            public void onError(Activity activity, String response, Exception e)
            {
                if (e != null)
                {
                    e.printStackTrace();
                }
                Utils.shortToast(MainActivity.this, R.string.text_error);
            }
            
            @Override
            public void onSuccess(Activity activity, JsonElement jsonElm)
            {
                progress.dismiss();
                if (!jsonElm.isJsonArray())
                {
                    System.err.println("Response is not a JSON array!");
                }
//                computeContainerStats(jsonElm.getAsJsonArray(), containers);
                new ComputeStatsTask(MainActivity.this, true).execute(jsonElm.getAsJsonArray());
            }
        });
        
        // Get comma-delimited list of container names.
        StringBuilder contNames = new StringBuilder();
        for (String contName : Globals.wcAll.keySet())
        {
            if (contNames.length() > 0)
            {
                contNames.append(",");
            }
            contNames.append(contName);
        }
        System.out.println("Container names: " + contNames.toString());
        reqTaskParam.addParam("names", contNames.toString());  
        HTTPGETTask.exec(reqTaskParam);
    }
    
    private void nextScreen(int position)
    {
        Class<?> clazz = null;
        boolean needsResult = false;
        switch (position)
        {
            case 0:
                clazz = StatsActivity.class;
                break;
            case 1:
                clazz = NotificationActivity.class;
                break;
            case 2:
                clazz = ReportProblemActivity.class;
                break;
            case 3:
                clazz = SettingsActivity.class;
                needsResult = true;
                break;
        }
        Intent intent = new Intent(this, clazz);
        if (needsResult)
        {
            startActivityForResult(intent, 0);
        }
        else
        {
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // We ignore the request code for now, since we only have one activity with results.
        if ((resultCode == Activity.RESULT_OK) && (Globals.selectedContainerGroupName != null))
        {
            Utils.saveData(this);
            fetchContainerList();
        }
    }
}
