package edu.upc.gessi.ecocity.activity;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import edu.upc.gessi.ecocity.R;
import edu.upc.gessi.ecocity.Utils;
import edu.upc.gessi.ecocity.http.HTTPGETTask;
import edu.upc.gessi.ecocity.http.HTTPReqTaskParam;
import edu.upc.gessi.ecocity.http.HTTPRespJSONCallback;
import edu.upc.gessi.ecocity.model.Globals;

public class SettingsActivity extends FragmentActivity
                              implements OnMapReadyCallback
{
    private static final LatLng LAT_LNG_BARCELONA = new LatLng(41.3862, 2.1824);
    
    private GoogleMap map;
    
    private Map<Marker,String> markers = new HashMap<Marker, String>();
    private Marker selectedMarker;
    private String selectedMarkerName;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        
        if (savedInstanceState != null)
        {
            selectedMarkerName = savedInstanceState.getString("selectedMarkerName");
            if ((selectedMarkerName != null) && (!selectedMarkerName.equals(Globals.selectedContainerGroupName)))
            {
                // There was a marker selected previously (that was different from the existing selected container group)
                // so enable the button.
                findViewById(R.id.button_select_this_location).setEnabled(true);
            }
        }
        else
        {
            selectedMarkerName = Globals.selectedContainerGroupName;
        }
        
//        Utils.shortToast(this, "Loading map...(selectedMarkerName : " + selectedMarkerName + ")");
        Utils.shortToast(this, this.getString(R.string.text_loading_map));

        Button selectButton = (Button)findViewById(R.id.button_select_this_location);
        selectButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (selectedMarker != null)
                {
                    if (!selectedMarkerName.equals(Globals.selectedContainerGroupName))
                    {
                        // Save the name of the selected container group.
                        Globals.selectedContainerGroupName = selectedMarkerName;
//                        Utils.shortToast(SettingsActivity.this, "Selected: " + Globals.selectedContainerGroupName);
                        setResult(Activity.RESULT_OK);
                    }
                    else
                    {
                        // Same container group as before was selected, so just cancel and exit.
                        setResult(Activity.RESULT_CANCELED);
                    }
                    // Close this activity.
                    SettingsActivity.this.finish();
                }
            }
        });
        
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putString("selectedMarkerName", selectedMarkerName);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed()
    {
        // Only allow going back if we already have a selected group name.
        if (Globals.selectedContainerGroupName != null)
        {
            super.onBackPressed();
        }
    }

    @Override
    public void onMapReady(GoogleMap map)
    {
        this.map = map;
        
        map.getUiSettings().setMapToolbarEnabled(false);

        // Set the map's center and zoom in.
        // TODO move camera based on user's location?
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LAT_LNG_BARCELONA, 16));
        
        map.setOnMarkerClickListener(new OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker)
            {
                if (!marker.equals(selectedMarker))
                {
                    if (selectedMarker != null)
                    {
                        // Deselect the previously selected marker.
                        selectedMarker.setIcon(getMarkerIcon(false));
                    }
                    selectedMarker = marker;
                    selectedMarker.setIcon(getMarkerIcon(true));
                    selectedMarkerName = markers.get(selectedMarker);
                    // Enable the "Select this location" button only if 
                    // the user pressed on a marker distinct from what
                    // is the currently saved container group.
                    findViewById(R.id.button_select_this_location).setEnabled(!selectedMarkerName.equals(Globals.selectedContainerGroupName));
                }
                
                return false;
            }
        });
        // Utils.shortToast(this, "Loading container locations...");
        String urlString = "http://silver-spring.appspot.com/containers/groups";
        HTTPReqTaskParam<JsonElement> taskParam = new HTTPReqTaskParam<JsonElement>(this, urlString, new HTTPRespJSONCallback() {
            
            @Override
            public void onError(Activity activity, String response, Exception e)
            {
                if (e != null)
                {
                    e.printStackTrace();
                }
                Utils.shortToast(SettingsActivity.this, R.string.text_error);
            }
            
            @Override
            public void onSuccess(Activity activity, JsonElement jsonElm)
            {
                if (!jsonElm.isJsonArray())
                {
                    System.err.println("Response is not a JSON array!");
                }
                
                // Mark location of container groups.
                JsonArray jsonArr = jsonElm.getAsJsonArray();
                for (JsonElement e : jsonArr)
                {
                    JsonObject jsonObj = e.getAsJsonObject();
                    addMarker(Utils.getDoubleProperty(jsonObj, "latitude"), 
                              Utils.getDoubleProperty(jsonObj, "longitude"),
                              Utils.getStringProperty(jsonObj, "groupName"));
                }
            }
        });
        
        HTTPGETTask.exec(taskParam);
    }
    
    private void addMarker(double latitude, double longitude, String containerGroupName)
    {
        boolean isSelectedMarker = containerGroupName.equals(selectedMarkerName);
        MarkerOptions markerOps = new MarkerOptions().position(new LatLng(latitude, longitude))
                                                     .icon(getMarkerIcon(isSelectedMarker));
        Marker marker = map.addMarker(markerOps);
        markers.put(marker, containerGroupName);
        if (isSelectedMarker)
        {
            selectedMarker = marker;
        }
    }
    
    private BitmapDescriptor getMarkerIcon(boolean isSelectedMarker)
    {
        return BitmapDescriptorFactory.defaultMarker(isSelectedMarker ? BitmapDescriptorFactory.HUE_CYAN : BitmapDescriptorFactory.HUE_RED);
    }
}
