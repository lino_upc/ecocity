package edu.upc.gessi.ecocity.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import android.annotation.SuppressLint;

@SuppressLint("SimpleDateFormat")
public class WasteContainerStats implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    private static final int MILLIS_IN_DAY = 24 * 60 * 60 * 1000;
    private static final DateFormat YEAR_DASH_MONTH_DASH_DAY = new SimpleDateFormat("yyyy-MM-dd");
    
    private WasteContainerType type;
    private double capacity;
    private double dailyAverage = 0.0;
    private double weeklyAverage = 0.0;
    private TreeMap<String,Double> wasteCollectedByDay = new TreeMap<String, Double>();

    public WasteContainerStats(WasteContainerType type, double capacity)
    {
        this.type = type;
        this.capacity = capacity;
    }
    
    public Map<String,Double> getWasteCollectedByDay()
    {
        return new LinkedHashMap<String,Double>(wasteCollectedByDay);
    }
    
    public void incWasteCollectedOn(String dateStr, double increment)
    {
        Double w = wasteCollectedByDay.get(dateStr);
        if (w == null)
        {
            w = increment;
        }
        else
        {
            w += increment;
        }
        System.out.println("+++ Inc " + type + " by " + increment + " on " + dateStr);
        wasteCollectedByDay.put(dateStr, w);
    }
    
    public double getTotalWasteCollected()
    {
        double totalWasteCollected = 0.0;
        for (Double w : wasteCollectedByDay.values())
        {
            totalWasteCollected += w;
        }
        return totalWasteCollected;
    }
    
    public String getFirstDateStr()
    {
        if (wasteCollectedByDay.isEmpty())
        {
            return null;
        }
        return wasteCollectedByDay.firstKey();
    }
    
    public String getLastDateStr()
    {
        if (wasteCollectedByDay.isEmpty())
        {
            return null;
        }
        return wasteCollectedByDay.lastKey();
    }
    
    public void compute()
    {
        int numDays = 0;
        int numWeeks = 0;
        String firstDateStr = getFirstDateStr();
        String lastDateStr = getLastDateStr();
        double totalWasteCollected = getTotalWasteCollected();
        // Given the first and last date strings, find out how many days are covered by the dataset.
        if (firstDateStr != null)
        {
            try
            {
                long firstDateMillis = YEAR_DASH_MONTH_DASH_DAY.parse(firstDateStr).getTime();
                long lastDateMillis = YEAR_DASH_MONTH_DASH_DAY.parse(lastDateStr).getTime();
                
                numDays = (int)((lastDateMillis - firstDateMillis) / MILLIS_IN_DAY) + 1;
                numWeeks = (int)Math.ceil(numDays / 7.0);
            }
            catch (ParseException e)
            {
                // Ignored
            }
        }
        
        System.out.println(lastDateStr + " - " + firstDateStr + " = " + numDays + " days (" + numWeeks + " weeks)");
        dailyAverage = ((numDays > 0) ? (totalWasteCollected / numDays) : 0);
        weeklyAverage = ((numWeeks > 0) ? (totalWasteCollected / numWeeks) : 0);
    }

    public double getDailyAverage()
    {
        return dailyAverage;
    }

    public double getWeeklyAverage()
    {
        return weeklyAverage;
    }

    public WasteContainerType getType()
    {
        return type;
    }

    public double getCapacity()
    {
        return capacity;
    }
}