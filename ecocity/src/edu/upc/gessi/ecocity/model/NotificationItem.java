package edu.upc.gessi.ecocity.model;

import java.io.Serializable;

public class NotificationItem implements Serializable
{
    private static final long serialVersionUID = 1L;

    public static enum Type
    {
        GENERAL_INFO,
        RECYCLING_STATS,
        PROBLEM_REPORTED;
    }
    private String text;
    private boolean read;
    private Type type;
    private long timestamp;

    public NotificationItem(String text, Type type)
    {
        this(text, type, System.currentTimeMillis());
    }
    
    public NotificationItem(String text, Type type, long timestamp)
    {
        this.text = text;
        this.read = false;
        this.type = type;
        this.timestamp = timestamp;
    }

    public String getText()
    {
        return text;
    }

    public boolean isRead()
    {
        return read;
    }

    public Type getType()
    {
        return type;
    }

    public long getTimestamp()
    {
        return timestamp;
    }
    
    public void setRead()
    {
        read = true;
    }
}
