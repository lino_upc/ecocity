package edu.upc.gessi.ecocity.model;

import android.annotation.SuppressLint;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class WasteContainer implements Serializable
{
    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat DATE_FORMAT_ISO8601 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

    private static final long serialVersionUID = 1L;
    
    private String name;
    private int capacity;
    private WasteContainerType type;
    private double latitude;
    private double longitude;
    
    private Long lastReadingTS;
    
    public WasteContainer(String name, WasteContainerType type, int capacity, 
                          double latitude, double longitude)
    {
        this.name = name;
        this.capacity = capacity;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.lastReadingTS = null;
    }

    public String getName()
    {
        return name;
    }

    public int getCapacity()
    {
        return capacity;
    }

    public WasteContainerType getType()
    {
        return type;
    }
    
    public double getLatitude()
    {
        return latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    // Parses the given string (in ISO 8601 format) and stores it iff it is later
    // than the last known reading for this container.
    public void setLastReading(String timestampStr)
    {
        try
        {
            long timestamp = DATE_FORMAT_ISO8601.parse(timestampStr).getTime();
            if ((lastReadingTS == null) || (lastReadingTS < timestamp))
            {
                lastReadingTS = timestamp;
            }
        }
        catch (ParseException e)
        {
            // Invalid timestamp string; ignore.
        }
    }
    
    // Returns true if the last reading for this container is earlier than
    // the given timestamp (String in ISO 8601 format); false otherwise.
    public boolean isLastReadingEarlierThan(String timestampStr)
    {
        try
        {
            long timestamp = DATE_FORMAT_ISO8601.parse(timestampStr).getTime();
            return ((lastReadingTS != null) && (lastReadingTS < timestamp));
        }
        catch (ParseException e)
        {
            // Invalid timestamp string, so return false.
        }
        return false;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
