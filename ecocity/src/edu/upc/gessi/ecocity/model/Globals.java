package edu.upc.gessi.ecocity.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Quick and dirty way to share data between Activities...implementing Parcelable
// is (arguably) the more "robust" solution.
public class Globals
{
    public static final String ICITY_API_KEY = "l7xx5d907cab477a494e8b9b2ef45f8d6b52";
    
    public static final String ICITY_311_API_KEY = "l7xx0e49dccf1a21422b9f2d2a1d54ed4700";
    
    public static final DecimalFormat DECI_FORMAT = new DecimalFormat("#,###.##");
    
    public static Map<WasteContainerType,WasteContainerStats> wcAllStats = new HashMap<WasteContainerType,WasteContainerStats>();

    public static String selectedContainerGroupName;
    
    public static List<NotificationItem> notifications = new ArrayList<NotificationItem>();
    
    public static Map<String,WasteContainer> wcAll = new HashMap<String,WasteContainer>();
}
