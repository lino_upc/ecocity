package edu.upc.gessi.ecocity.model;

public enum WasteContainerType
{
    WASTE(0),
    PAPER(1),
    GLASS(2),
    PLASTIC(3),
    ORGANIC(4);

    private int typeId;
    
    private WasteContainerType(int typeId)
    {
        this.typeId = typeId;
    }
    
    public static WasteContainerType valueOf(int typeId)
    {
        for (WasteContainerType wcType : values())
        {
            if (wcType.typeId == typeId)
            {
                return wcType;
            }
        }
        return null;
    }
    
    public int getId()
    {
        return typeId;
    }
}