package edu.upc.gessi.ecocity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MainScreenImageAdapter extends ArrayAdapter<LabelledImage>
{
    public MainScreenImageAdapter(Context context,
                                  LabelledImage[] mainScreenImages)
    {
        super(context, R.layout.grid_item_layout, mainScreenImages);
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) 
    {
        if(convertView == null)
        {
            LayoutInflater i = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View)i.inflate(R.layout.grid_item_layout, parent, false);
        }

        // Create an image and a text view for the item in the current position.
        ImageView i = (ImageView) convertView.findViewById(R.id.image);
        i.setImageResource(getItem(position).getImageResId());

        TextView t = (TextView) convertView.findViewById(R.id.label);
        t.setText(getContext().getString(getItem(position).getLabelResId()));

        return convertView;
    }    
}
