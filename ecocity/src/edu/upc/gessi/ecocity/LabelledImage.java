package edu.upc.gessi.ecocity;

public class LabelledImage
{
    private int imageResId;
    private int labelResId;
    
    public LabelledImage(int imageResId, int labelResId)
    {
        this.imageResId = imageResId;
        this.labelResId = labelResId;
    }

    public int getImageResId()
    {
        return imageResId;
    }

    public int getLabelResId()
    {
        return labelResId;
    }
}